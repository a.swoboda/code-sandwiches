package de.muc.swoboda.andreas;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
    static void copyEveryOtherLine() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("foo.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("bar.txt"));
        for(String line; (line=br.readLine()) != null; br.readLine()) {
            bw.write(line + "\n");
        }
    }

    public static void main(String[] args) throws IOException {
        copyEveryOtherLine();
    }
}
