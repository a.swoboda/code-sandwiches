package de.muc.swoboda.andreas;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Fallback {

    static void copyEveryOtherLine0() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("foo.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("bar.txt"));
        for(String line; (line=br.readLine()) != null; br.readLine()) {
            bw.write(line + "\n");
        }
    }

    static void copyEveryOtherLine1() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("foo.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("bar.txt"));
        for(String line; (line=br.readLine()) != null; br.readLine()) {
            bw.write(line + "\n");
        }
        bw.close();
        br.close();
    }

    static void copyEveryOtherLine2() throws IOException {
        try(BufferedReader br = new BufferedReader(new FileReader("foo.txt"));
            BufferedWriter bw = new BufferedWriter(new FileWriter("bar.txt"))) {
            for (String line; (line = br.readLine()) != null; br.readLine()) {
                bw.write(line + "\n");
            }
        }
    }

    static void copyEveryOtherLine3() throws IOException {
        try(FileReader fr = new FileReader("foo.txt");
            BufferedReader br = new BufferedReader(fr);
            FileWriter fw = new FileWriter("bar.txt");
            BufferedWriter bw = new BufferedWriter(fw)){
            for(String line; (line=br.readLine())!=null; br.readLine()) {
                bw.write(line + "\n");
            }
        }
    }
    
}
